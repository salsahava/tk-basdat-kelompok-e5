from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect,HttpResponse,JsonResponse
from django.urls import reverse
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt


def Acara():
	response = {}
	cursor = connection.cursor()

	cursor.execute('SET search_path to BIKE_SHARING, public')
    cursor.execute("SELECT nama FROM STASIUN;")
    data = cursor.fetchall()

    response ['namastasiun'] = data
    print(response)    
    return render(request, 'createAcara.html', response)


def daftarAcara(request):
	response = {}
	cursor = connection.cursor()

	cursor.execute('SET search_path to BIKE_SHARING, public')
    cursor.execute("SELECT A.judul, A.deskripsi, A.is_free, A.tgl_mulai, A.tgl_akhir, S.id_stasiun, S.nama FROM ACARA A, STASIUN S, ACARA_STASIUN ACS WHERE ACS.id_stasiun = S.id_stasiun AND ACS.id_acara = A.id_acara;")
    resp = cursor.fetchall()
    response['acara'] = resp

    return render(request, 'daftarAcara.html', response)

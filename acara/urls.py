from django.urls import path
from django.conf.urls import url
from . import views
from .views import Acara, daftarAcara

app_name = 'acara'

urlpatterns = [
	path('acara/', views.Acara, name='acara')
	path('acara/daftarAcara', views.daftarAcara, name='daftarAcara')
]
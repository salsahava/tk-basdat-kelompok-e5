from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect

def Penugasan(request):
	response = {}
	cursor = connection.cursor()

	cursor.execute('SET search_path to BIKE_SHARING, public')
	cursor.execute("SELECT PN.ktp, PR.nama, PN.start_datetime, PN.id_stasiun, S.nama FROM PENUGASAN PN, PERSON PR, STASIUN S, PETUGAS PT WHERE PN.ktp = PT.ktp AND S.id_stasiun = PN.id_stasiun AND PR.ktp = PT.ktp;")
	resp = cursor.fetchall()
	response['Penugasan'] = resp

	return render(request, 'daftarPenugasan.html', response)
from django.urls import path
from django.conf.urls import url
from . import views
from .views import Penugasan

app_name = 'penugasan'

urlpatterns = [
	path('penugasan/', views.Penugasan, name='penugasan')
]